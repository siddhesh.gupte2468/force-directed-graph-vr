﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLinesBetweenNodes : MonoBehaviour
{

    public Material lineMat;
    public Material selectedLineMat;
    public List<Node> nodes;
    public GraphPlotter graphscript;

    // Selected node's links should be made the same color
    public GameObject selectedNodeBody;
    public GameObject previousSelectedNodeBody;

    // Use this for initialization
    void Start()
    {
        if (!graphscript)
            graphscript = GameObject.Find("Graph").GetComponent<GraphPlotter>();
        nodes = graphscript.nodes;
    }
    
    void DrawLines()
    {
        foreach (Node node in nodes)
        {
            Vector3 parentNodePosition = node.body.transform.position;
            foreach (Node childnode in node.children)
            {
                Vector3 childNodePosition = childnode.body.transform.position;

                GL.Begin(GL.LINES);
                lineMat.SetPass(0);
                //lineMat.color = childnode.body.GetComponent<Renderer>().material.color;
                GL.Color(new Color(lineMat.color.r, lineMat.color.g, lineMat.color.b, lineMat.color.a));
                GL.Vertex3(parentNodePosition.x, parentNodePosition.y, parentNodePosition.z);
                GL.Vertex3(childNodePosition.x, childNodePosition.y, childNodePosition.z);
                GL.End();
            }
        }
    }

    void DrawLinesFromSelectedNode()
    {
        foreach (Node node in nodes)
        {
            if (node.body == selectedNodeBody)
            {
                Vector3 parentNodePosition = node.body.transform.position;
                foreach (Node childnode in node.children)
                {
                    if (selectedNodeBody != previousSelectedNodeBody)

                        DisableTags();

                    // Set name tags as active for all child objects
                    childnode.body.transform.GetChild(0).gameObject.SetActive(true);

                    Vector3 childNodePosition = childnode.body.transform.position;
                    GL.Begin(GL.LINES);
                    selectedLineMat.SetPass(0);
                    GL.Color(new Color(selectedLineMat.color.r, selectedLineMat.color.g, selectedLineMat.color.b, selectedLineMat.color.a));
                    GL.Vertex3(parentNodePosition.x, parentNodePosition.y, parentNodePosition.z);
                    GL.Vertex3(childNodePosition.x, childNodePosition.y, childNodePosition.z);
                    GL.End();
                }
            }
        }
        previousSelectedNodeBody = selectedNodeBody;
    }

    public void DisableTags()
    {
        foreach (Node node1 in nodes)
        {
            if (node1.body == previousSelectedNodeBody)
            {
                foreach (Node subnode in node1.children)
                {
                    subnode.body.transform.GetChild(0).gameObject.SetActive(false);
                }
            }
        }
    }

    private void OnPostRender()
    {
        nodes = graphscript.nodes;
        DrawLines();
        if (selectedNodeBody)
        {
            DrawLinesFromSelectedNode();
        }

    }

    public void ChangeColorOfChildNodes()
    {
        foreach (Node node in nodes)
        {
            if (node.body == selectedNodeBody)
            {
                foreach(Node childnode in node.children)
                {
                    childnode.body.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
                }
            }
        }
    }
}