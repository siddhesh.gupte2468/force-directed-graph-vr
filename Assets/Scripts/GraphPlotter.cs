﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GraphPlotter : MonoBehaviour
{

    public float desiredConnectedNodeDistance = 1;
    public float connectedNodeForce = 1;
    public float disconnectedNodeForce = 1;
    public GameObject spherePrefab;
    public Transform pointHolder;

    public List<Node> nodes;

    private bool areTagsActive;

    // Name of the input file, no extension
    public string inputfile;


    // List for holding data from CSV reader
    private List<Dictionary<string, object>> pointList;
    private Dictionary<string, List<string>> nameDictionary;
    private List<string> nameList;

    // Use this for initialization
    void Start()
    {
        areTagsActive = true;
        pointHolder.localPosition = new Vector3(0, 4f, 0);

        // Set pointlist to results of function Reader with argument inputfile
        pointList = CSVReader.Read(inputfile);
            
        Dictionary<string, List<string>> nameDictionary = new Dictionary<string, List<string>>();
        // Setup the name dictionary
        for (int i = 0; i < pointList.Count; i++)
        {
            // Some nodes are not calling, only recieving calls
            List<string> result;
            if(!nameDictionary.TryGetValue(pointList[i]["Person"].ToString(), out result))
            {
                result = new List<string>();
                nameDictionary[pointList[i]["Person"].ToString()] = result;
            }
            var lst = nameDictionary[pointList[i]["Person"].ToString()];

            if(!lst.Contains(pointList[i]["telecom.receivername"].ToString()))
                lst.Add(pointList[i]["telecom.receivername"].ToString());
            nameDictionary[pointList[i]["Person"].ToString()] = lst;

            // Recieving nodes should only be initialized in this pass
            if (!nameDictionary.TryGetValue(pointList[i]["telecom.receivername"].ToString(), out result))
            {
                result = new List<string>();
                nameDictionary[pointList[i]["telecom.receivername"].ToString()] = result;
            }
        }

        foreach(KeyValuePair<string, List<string>> pair in nameDictionary)
        {
            Debug.Log(string.Format("{0} {1}", pair.Key, pair.Value));
            foreach(var name in pair.Value)
            {
                Debug.Log("......" + name);
            }
        }

        // List of names from the nameDictionary
        nameList = new List<string>(nameDictionary.Keys);

        // THIS IS WHERE NODES ARE MADE
        nodes = new List<Node>();

        for (int i = 0; i < nameDictionary.Count; i++)
        {
            Node n = new Node()
            {
                children = nodes.Where(node => Random.value > 0.5f).ToList(),
                velocity = Vector3.zero,
                body = Instantiate(spherePrefab, pointHolder, false),
            };
            n.body.transform.localPosition = Random.insideUnitSphere * 4f;
            n.rb = n.body.GetComponent<Rigidbody>();
            n.body.GetComponentInChildren<TextMesh>().text = nameList[i];
            n.body.GetComponent<Renderer>().material.color = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));

            //float numofchildren = nameDictionary[nameList[i]].Count;
            float numofchildren = n.children.Count;
            n.body.transform.localScale = new Vector3(0.5f+0.1f*numofchildren, 0.5f + 0.2f * numofchildren, 0.5f + 0.1f * numofchildren);
            nodes.Add(n);
        }
    }

    // Update is called once per frame
    void Update()
    {

        ApplyGraphForce();

        foreach (Node node in nodes)
        {
            node.body.transform.localPosition += node.velocity * Time.deltaTime;
        }

    }

    public string ReturnNodeName(GameObject selectedGameObject)
    {
        foreach (Node node in nodes)
        {
            if (node.body == selectedGameObject)
                return node.body.transform.GetChild(0).gameObject.GetComponent<TextMesh>().text;
        }
        return "Selected object not in nodes list";  
    }

    private void ApplyGraphForce()
    {
        foreach (var node in nodes)
        {
            var disconnectedNodes = nodes.Except(node.children);

            foreach (var connectedNode in node.children)
            {
                var difference = node.body.transform.position - connectedNode.body.transform.position;
                var distance = (difference).magnitude;
                var appliedForce = connectedNodeForce * Mathf.Log10(distance / desiredConnectedNodeDistance);

                connectedNode.rb.velocity += appliedForce * Time.deltaTime * difference.normalized;
            }

            foreach (var disconnectedNode in disconnectedNodes)
            {
                var difference = node.body.transform.position - disconnectedNode.body.transform.position;
                float distance = (difference).magnitude;
                if (distance == 0)
                {
                    distance = 0.0001f;
                }
                float appliedForce = disconnectedNodeForce / Mathf.Pow(distance, 2);
                disconnectedNode.rb.velocity += appliedForce * Time.deltaTime * -difference.normalized;
            }
        }
    }

    public void ToggleNamesForNodes()
    {
        areTagsActive = !areTagsActive;
        foreach (Node node in nodes)
        {
            GameObject nameTag = node.body.transform.GetChild(0).gameObject;
            nameTag.SetActive(areTagsActive);
        }
    }
}
//    private void OnDrawGizmos()
//    {
//        foreach(var node in nodes)
//        {
//            //Gizmos.color = Color.red;
//            //Gizmos.DrawSphere(node.position, 0.25f);

//            Gizmos.color = Color.red;
//            foreach(Node connectedNode in node.children)
//            {
//                Gizmos.DrawLine(node.body.transform.position, connectedNode.body.transform.position);
//            }
//        }
//    }
//}



public class Node
{
    //public Vector3 position;
    public Vector3 velocity;
    public List<Node> children;
    public GameObject body;
    public Rigidbody rb;
}