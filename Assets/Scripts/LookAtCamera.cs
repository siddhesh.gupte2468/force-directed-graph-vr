﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour {

    // Update is called once per frame
    void Update () {
        // Look at camera
        transform.LookAt(2 * transform.position - Camera.main.transform.position);
    }

}
