﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class raycastPoint : MonoBehaviour {

    // Cache material for shader
    Material cachedMaterial;

    //centre of screen
    private Camera fpsCam;
    private LineRenderer laserLine;
    bool pointerActivate = false;
    public TextMesh screenText;
    public TextMesh distanceText;

    // To restore color
    GameObject obj1; // previous object
    Renderer objRenderer1; // previous object renderer
    Color oldcolor1; // previous color

    DrawLinesBetweenNodes drawbetweennodesscript;
    GraphPlotter graphplotterscript;
    
    private void Awake()
    {
        cachedMaterial = new Material(Shader.Find("Particles/Additive"));
    }

    void Start () {
        fpsCam = GetComponentInParent<Camera>();
        laserLine = GetComponent<LineRenderer>();

        // Set line material and color
        laserLine.startColor = Color.red;
        laserLine.endColor = Color.red;

        screenText.text = "";
        distanceText.text = "";

        drawbetweennodesscript = GameObject.Find("FirstPersonCharacter").GetComponent<DrawLinesBetweenNodes>();
        graphplotterscript = GameObject.Find("GraphPlotter").GetComponent<GraphPlotter>();
    }

    void Update () {
        RaycastHit hit;

        // centre of screen
        Vector3 rayOrigin = fpsCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
        Debug.DrawRay(rayOrigin, fpsCam.transform.forward * 10, Color.green);


        // laserLine material and startposition
        laserLine.SetPosition(0, rayOrigin + new Vector3(0.2f, 0, -0.1f));
        laserLine.material = cachedMaterial;

        if (Input.GetMouseButtonDown(0) || Input.GetButtonDown("Fire3"))
            pointerActivate = !laserLine.enabled;

        if (Physics.Raycast(rayOrigin, fpsCam.transform.forward, out hit, maxDistance: Mathf.Infinity, layerMask: 1 << 9) && hit.transform.tag == "sphereobj")
        {

            if (obj1 != hit.collider.gameObject)//If we're not pointing at the previous target
            {
                if (obj1 != null)//If previous target is set, reset its color
                {
                    objRenderer1.material.SetColor("_Color", oldcolor1);
                }
                obj1 = hit.collider.gameObject; // Store current object for next update
                objRenderer1 = obj1.GetComponent<Renderer>(); //Get targets Renderer
                oldcolor1 = objRenderer1.material.GetColor("_Color"); //Store targets current color
                objRenderer1.material.SetColor("_Color", Color.red); //Set target to new color

            }

            laserLine.SetPosition(1, hit.point);
            laserLine.enabled = pointerActivate;

            // Set screen text in the format x: 0.0, y: 0.0, z: 0.0
            //string[] tokens = hit.collider.gameObject.name.Split();
            //screenText.text = string.Format("x: {0}, y: {1}, z: {2}",
            //                                tokens[0],
            //                                tokens[1],
            //                                tokens[2]);
            screenText.text = graphplotterscript.ReturnNodeName(hit.collider.gameObject);
        }
        else
        {//If we're not pointing at anything
            if (obj1 != null)
            {
                objRenderer1.material.SetColor("_Color", oldcolor1);//Reset targets material
                obj1 = null;//Clear reference
            }

            laserLine.SetPosition(1, rayOrigin + (fpsCam.transform.forward * 20));
            laserLine.enabled = pointerActivate;

            screenText.text = "";
        }

        // Node selection
        if (Input.GetKeyDown(KeyCode.E) || Input.GetButtonDown("Fire1"))
        {
            if (Physics.Raycast(rayOrigin, fpsCam.transform.forward, out hit, maxDistance: Mathf.Infinity, layerMask: 1 << 9) && hit.transform.tag == "sphereobj")
            {
                GameObject registeredObj = hit.collider.gameObject;
                distanceText.text = graphplotterscript.ReturnNodeName(registeredObj);

                // Assigning the selectedNodeBody will allow DrawLinesFromSelectedNode() function to be executed in DrawLinesBetweenNodes.cs automatically
                drawbetweennodesscript.selectedNodeBody = registeredObj;

                //// Change all child nodes colors
                //drawbetweennodesscript.ChangeColorOfChildNodes();
                
            }

        }

        // Reset selectedNodeBody
        if (Input.GetKeyDown(KeyCode.R) || Input.GetButtonDown("Fire2"))
        {
            distanceText.text = "";
            drawbetweennodesscript.DisableTags();
            drawbetweennodesscript.selectedNodeBody = null;
        }

        // Toggle Names for nodes
        if (Input.GetKeyDown(KeyCode.T))
        {
            graphplotterscript.ToggleNamesForNodes();
        }
    }
}
